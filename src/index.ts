import init from './server';

init().then((srv) => {
  srv.start().then(() => console.log('server is ready'));
});