import * as Boom  from '@hapi/boom';
import * as hapi from '@hapi/hapi';
import * as joi from 'joi';
import * as fields from './lib/fields';
import * as users from './lib/users';

const routes: hapi.ServerRoute[] = [
  {
    method: 'POST',
    path: '/file',
    options: {
      payload: {
        maxBytes: 100,
        multipart: {
          output: 'file'
        }
      }
    },
    handler: (request: hapi.Request) => {
      return JSON.stringify(request.payload)
    }
  },
  {
    method: 'GET',
    path: '/static/{data*}',
    handler: {
      directory: {
        listing: true,
        path: './'
      }
    }
  },
  {
    method: 'GET',
    path: '/field',
    options: {
      tags: ['api'],
      auth: 'userauth'
    },
    handler: fields.getFields
  },
  {
    method: 'GET',
    path: '/field/{id}',
    options: {
      auth: {
        strategy: 'userauth',
        scope: ['admin']
      }
    },
    handler: (req: hapi.Request) => {
      // const cred: any = req.auth.credentials;
      // return cred;
      return fields.getField(req.params.id).field;
    }
  },
  {
    method: 'PUT',
    path: '/field/{fieldId}/move',
    options: {
      tags: ['api'],
      description: 'сделать ход на поле',
      validate: {
         payload: joi.object({
           x: joi.number().required().min(0).max(2),
           y: joi.number().required().min(0).max(2),
           player: joi.number().required().min(1).max(2)
         }),
         params: joi.object({
          fieldId: joi.number()
         })
      }
    },
    handler: (req: hapi.Request) => {
      const body = req.payload as any;
      fields.makeMove(req.params.fieldId, body.x, body.y, body.player);
      return 'ok';
    }
  },
  {
    method: 'POST',
    path: '/login',
    options: {
      validate: {
        payload: joi.object({
          login: joi.string().required(),
          password: joi.string().required()
        })
      }
    },
    handler: (request, h) => {
      const payload: any = request.payload;
      return users.login(payload.login, payload.password);
    }
  }
];

export default routes;