import * as hapi from '@hapi/hapi';

import * as inert from '@hapi/inert';
import * as vision from '@hapi/vision';
import * as swagger from 'hapi-swagger';
import * as nowAuth from '@now-ims/hapi-now-auth';

import * as users from './lib/users'; 

import routes from './routes';

export default function() {
    const srv = hapi.server({
        port: 3000,
        routes: {
          cors: {
            origin: ['*']
          },
          files: {
            relativeTo: './static' 
          }
        }
      });
      
      
      const plugins: any[] = [
        inert,
        nowAuth
      ];
      
      if (process.env.NODE_ENV === 'dev') {
        plugins.push(vision);
        plugins.push(swagger);
      }
      
      return srv.register(plugins).then(() => {
        srv.auth.strategy('userauth', 'hapi-now-auth', {
          validate: (request, token, h) => {
            const user = users.getSession(token);
            const scopes = [];
      
            if (user) {
              if (user.isAdmin) {
                scopes.push('admin');
              }
              return {
                isValid: true,
                credentials: {
                  scope: scopes,
                  userId: user.userId,
                  isAdmin: user.isAdmin,
                }
              }
            } else {
              return {
                isValid: false,
                credentials: {}
              }
            }
          }
        });
      
        srv.route(routes);

        return srv;
      });
}
