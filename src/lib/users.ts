const users = [
    {
        id: 1,
        login: 'one',
        password: 'pass1',
        isAdmin: true
    },
    {
        id: 2,
        login: 'two',
        password: 'pass2'
    },
    {
        id: 3,
        login: 'admin',
        password: 'admin',
        isAdmin: true
    },
];

interface ISession {
    userId: number;
    isAdmin: boolean;
}

const sessions: Map<string, ISession> = new Map();

export function login(login: string, password: string): null | string {
    const user = users.find(el => el.login === login && el.password === password);

    if (user) {
        const sessionId = Math.random().toString();
        sessions.set(sessionId, {
            isAdmin: user.isAdmin,
            userId: user.id
        });

        return sessionId;
    } else {
        return null;
    }
}

export function getSession(sessionId: string): ISession | null {
    return sessions.get(sessionId);
}