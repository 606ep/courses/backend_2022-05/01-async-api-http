import init from '../../server';

test('Возвращается поле', async () => {
    const srv = await init();
        
    try {
        await srv.start()
        const response = await srv.inject({
            url: '/field/1',
            method: 'GET'
        });
        expect(response.result).toEqual([['', '', ''],
            ['', 'x', ''],
            ['', '', '']]);
    } finally {
        srv.stop();
    }
});


