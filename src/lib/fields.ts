const fields = [
    {
      name: 'game one',
      field: [
          ['', '', ''],
          ['', '', ''],
          ['', '', '']
      ]
      },
      {
      name: 'game two',
      field: [
          ['', '', ''],
          ['', 'x', ''],
          ['', '', '']
      ]
    },      
  ];

export function getFields() {
    return fields;
}

export function getField(id: number) {
    return fields[id];
}

export function makeMove(fieldId: number, x: number, y: number, player: number) {
    fields[fieldId].field[x][y] = player == 1 ? 'x' : 'o';
}