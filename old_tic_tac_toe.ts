import * as http from 'http';

let field = [
    ['0', 'X', ''],
    ['', '0', ''],
    ['', '', 'X']
];

// получить поле
// GET  /field

// очистить
// POST /field/clean

// сходить
// POST /field/makeMove
// {x:number, y:number, user: 1 | 2}


const srv = new http.Server((req, res) => {
    const { method, url } = req;

    if (method == 'GET' && url == '/field') {
        res.end(JSON.stringify(field));
    } else if (method == 'POST' && url == '/field/clean') {

        res.statusCode = 200;
        field = [
            ['', '', ''],
            ['', '', ''],
            ['', '', '']
        ];
        res.end('field clean');

    } else if (method == 'POST' && url == '/field/makeMove') {
        const dataArr = [];
        req.on('data', (chunk) => {
            dataArr.push(chunk);
        })

        req.on('end', () => {
            const dataStr = Buffer.concat(dataArr).toString();
            const data = JSON.parse(dataStr);
            console.log(data.x, data.y, data.player);
            field[data.x][data.y] = data.player;

            
            res.end('move');
        })
    } else {
        res.statusCode = 404;
        res.end('not found');
    }
});



srv.listen(3000);